import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { GuaireCursosSharedModule } from 'app/shared';
import {
    GrupoComponent,
    GrupoDetailComponent,
    GrupoUpdateComponent,
    GrupoDeletePopupComponent,
    GrupoDeleteDialogComponent,
    grupoRoute,
    grupoPopupRoute
} from './';

const ENTITY_STATES = [...grupoRoute, ...grupoPopupRoute];

@NgModule({
    imports: [GuaireCursosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [GrupoComponent, GrupoDetailComponent, GrupoUpdateComponent, GrupoDeleteDialogComponent, GrupoDeletePopupComponent],
    entryComponents: [GrupoComponent, GrupoUpdateComponent, GrupoDeleteDialogComponent, GrupoDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuaireCursosGrupoModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
