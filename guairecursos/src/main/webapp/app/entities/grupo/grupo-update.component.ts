import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IGrupo } from 'app/shared/model/grupo.model';
import { GrupoService } from './grupo.service';
import { ICurso } from 'app/shared/model/curso.model';
import { CursoService } from 'app/entities/curso';

@Component({
    selector: 'jhi-grupo-update',
    templateUrl: './grupo-update.component.html'
})
export class GrupoUpdateComponent implements OnInit {
    grupo: IGrupo;
    isSaving: boolean;

    cursos: ICurso[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected grupoService: GrupoService,
        protected cursoService: CursoService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ grupo }) => {
            this.grupo = grupo;
        });
        this.cursoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ICurso[]>) => mayBeOk.ok),
                map((response: HttpResponse<ICurso[]>) => response.body)
            )
            .subscribe((res: ICurso[]) => (this.cursos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.grupo.id !== undefined) {
            this.subscribeToSaveResponse(this.grupoService.update(this.grupo));
        } else {
            this.subscribeToSaveResponse(this.grupoService.create(this.grupo));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IGrupo>>) {
        result.subscribe((res: HttpResponse<IGrupo>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCursoById(index: number, item: ICurso) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }

    nuevoCurso(curso) {
        console.log(curso.explicitOriginalTarget.innerHTML);

        if (this.grupo.orden === undefined) {
            this.grupo.orden = '';
        }

        this.grupo.orden += curso.explicitOriginalTarget.innerHTML + ';';
    }
}
