import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ICurso } from 'app/shared/model/curso.model';

import { IGrupo } from 'app/shared/model/grupo.model';

@Component({
    selector: 'jhi-grupo-detail',
    templateUrl: './grupo-detail.component.html'
})
export class GrupoDetailComponent implements OnInit {
    grupo: IGrupo;

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected dataUtils: JhiDataUtils
    ) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ grupo }) => {
            this.grupo = grupo;
            const itens = [];
            const orden = grupo.orden.split(';');
            for (let i = 0; i < orden.length; i++) {
                for (let j = 0; j < grupo.cursos.length; j++) {
                    const iten = grupo.cursos[j];
                    if (iten.nombre === orden[i]) {
                        itens.push(iten);
                    }
                }
            }
            this.grupo.cursos = itens;
        });
    }

    previousState() {
        window.history.back();
    }

    trackId(index: number, item: ICurso) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    calculateFate(a) {
        console.log(a);
        const fechita = new Date(a);
        if (fechita) {
            console.log(fechita.getTime() < new Date().getTime());
            console.log(fechita);
            if (fechita.getTime() < new Date().getTime()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
