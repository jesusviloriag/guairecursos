import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { ICurso } from 'app/shared/model/curso.model';
import { CursoService } from './curso.service';
import { IUser, UserService } from 'app/core';
import { IMoneda } from 'app/shared/model/moneda.model';
import { MonedaService } from 'app/entities/moneda';
import { ITipoPago } from 'app/shared/model/tipo-pago.model';
import { TipoPagoService } from 'app/entities/tipo-pago';

@Component({
    selector: 'jhi-curso-update',
    templateUrl: './curso-update.component.html'
})
export class CursoUpdateComponent implements OnInit {
    curso: ICurso;
    isSaving: boolean;

    users: IUser[];

    monedas: IMoneda[];

    tipopagos: ITipoPago[];
    fecha: string;

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected cursoService: CursoService,
        protected userService: UserService,
        protected monedaService: MonedaService,
        protected tipoPagoService: TipoPagoService,
        protected elementRef: ElementRef,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ curso }) => {
            this.curso = curso;
            this.fecha = this.curso.fecha != null ? this.curso.fecha.format(DATE_TIME_FORMAT) : null;
        });
        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.monedaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMoneda[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMoneda[]>) => response.body)
            )
            .subscribe((res: IMoneda[]) => (this.monedas = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.tipoPagoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ITipoPago[]>) => mayBeOk.ok),
                map((response: HttpResponse<ITipoPago[]>) => response.body)
            )
            .subscribe((res: ITipoPago[]) => (this.tipopagos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.curso, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.curso.fecha = this.fecha != null ? moment(this.fecha, DATE_TIME_FORMAT) : null;
        if (this.curso.id !== undefined) {
            this.subscribeToSaveResponse(this.cursoService.update(this.curso));
        } else {
            this.subscribeToSaveResponse(this.cursoService.create(this.curso));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ICurso>>) {
        result.subscribe((res: HttpResponse<ICurso>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    trackMonedaById(index: number, item: IMoneda) {
        return item.id;
    }

    trackTipoPagoById(index: number, item: ITipoPago) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}
