import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { ICurso } from 'app/shared/model/curso.model';

@Component({
    selector: 'jhi-curso-detail',
    templateUrl: './curso-detail.component.html'
})
export class CursoDetailComponent implements OnInit {
    curso: ICurso;

    fechaInvalida: boolean;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ curso }) => {
            this.curso = curso;

            if (curso.fecha._d.getTime() < new Date().getTime()) {
                this.fechaInvalida = true;
            } else {
                this.fechaInvalida = false;
            }
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
