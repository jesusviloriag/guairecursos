import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'curso',
                loadChildren: './curso/curso.module#GuaireCursosCursoModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'pago',
                loadChildren: './pago/pago.module#GuaireCursosPagoModule'
            },
            {
                path: 'curso',
                loadChildren: './curso/curso.module#GuaireCursosCursoModule'
            },
            {
                path: 'moneda',
                loadChildren: './moneda/moneda.module#GuaireCursosMonedaModule'
            },
            {
                path: 'curso',
                loadChildren: './curso/curso.module#GuaireCursosCursoModule'
            },
            {
                path: 'estatus-pago',
                loadChildren: './estatus-pago/estatus-pago.module#GuaireCursosEstatusPagoModule'
            },
            {
                path: 'pago',
                loadChildren: './pago/pago.module#GuaireCursosPagoModule'
            },
            {
                path: 'pago',
                loadChildren: './pago/pago.module#GuaireCursosPagoModule'
            },
            {
                path: 'inscripcion',
                loadChildren: './inscripcion/inscripcion.module#GuaireCursosInscripcionModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'pago',
                loadChildren: './pago/pago.module#GuaireCursosPagoModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'tipo-cuenta',
                loadChildren: './tipo-cuenta/tipo-cuenta.module#GuaireCursosTipoCuentaModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'tipo-cuenta',
                loadChildren: './tipo-cuenta/tipo-cuenta.module#GuaireCursosTipoCuentaModule'
            },
            {
                path: 'moneda',
                loadChildren: './moneda/moneda.module#GuaireCursosMonedaModule'
            },
            {
                path: 'curso',
                loadChildren: './curso/curso.module#GuaireCursosCursoModule'
            },
            {
                path: 'curso',
                loadChildren: './curso/curso.module#GuaireCursosCursoModule'
            },
            {
                path: 'curso',
                loadChildren: './curso/curso.module#GuaireCursosCursoModule'
            },
            {
                path: 'inscripcion',
                loadChildren: './inscripcion/inscripcion.module#GuaireCursosInscripcionModule'
            },
            {
                path: 'inscripcion',
                loadChildren: './inscripcion/inscripcion.module#GuaireCursosInscripcionModule'
            },
            {
                path: 'pago',
                loadChildren: './pago/pago.module#GuaireCursosPagoModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'tipo-pago',
                loadChildren: './tipo-pago/tipo-pago.module#GuaireCursosTipoPagoModule'
            },
            {
                path: 'grupo',
                loadChildren: './grupo/grupo.module#GuaireCursosGrupoModule'
            },
            {
                path: 'grupo',
                loadChildren: './grupo/grupo.module#GuaireCursosGrupoModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuaireCursosEntityModule {}
