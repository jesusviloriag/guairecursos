import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInscripcion } from 'app/shared/model/inscripcion.model';

@Component({
    selector: 'jhi-inscripcion-detail',
    templateUrl: './inscripcion-detail.component.html'
})
export class InscripcionDetailComponent implements OnInit {
    inscripcion: IInscripcion;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ inscripcion }) => {
            this.inscripcion = inscripcion;
        });
    }

    previousState() {
        window.history.back();
    }
}
