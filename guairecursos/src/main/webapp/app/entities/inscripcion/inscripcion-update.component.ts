import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IInscripcion } from 'app/shared/model/inscripcion.model';
import { InscripcionService } from './inscripcion.service';
import { ICurso } from 'app/shared/model/curso.model';
import { IPago } from 'app/shared/model/pago.model';
import { ITipoPago } from 'app/shared/model/tipo-pago.model';
import { CursoService } from 'app/entities/curso';
import { IUser, UserService } from 'app/core';
import { AccountService, Account } from 'app/core';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';

@Component({
    selector: 'jhi-inscripcion-update',
    templateUrl: './inscripcion-update.component.html'
})
export class InscripcionUpdateComponent implements OnInit {
    public payPalConfig?: IPayPalConfig;

    pago: IPago;

    inscripcion: IInscripcion;
    isSaving: boolean;

    isPaid: boolean;

    paypalId: string;
    price: string;

    users: IUser[];
    fecha: string;

    cambio: any;

    varios: boolean;

    cantidadAlumnos: number;

    montoOriginal: number;

    deseaDonar: boolean;

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected inscripcionService: InscripcionService,
        protected cursoService: CursoService,
        protected userService: UserService,
        protected activatedRoute: ActivatedRoute,
        private accountService: AccountService,
        protected elementRef: ElementRef
    ) {}

    ngOnInit() {
        this.varios = false;

        this.cantidadAlumnos = 2;

        this.isSaving = false;

        this.paypalId = 'AcnlYadc2ABqHZvnOWgGz1A74bd0BNFkJtZsL083Y816fNz3A8M9M4Ql205s4EVdzLl5XIv2xpbTUOMm';

        this.payPalConfig = {
            currency: 'USD',
            clientId: this.paypalId,
            createOrderOnClient: data =>
                <ICreateOrderRequest>{
                    intent: 'CAPTURE',
                    purchase_units: [
                        {
                            amount: {
                                value: this.price.toString()
                            }
                        }
                    ]
                },
            advanced: {
                commit: 'true'
            },
            style: {
                label: 'paypal',
                layout: 'vertical'
            },
            onApprove: (data, actions) => {
                console.log('onApprove - transaction was approved, but not authorized', data, actions);
                alert('Transacción exitosa');
                actions.order.get().then(details => {
                    console.log('onApprove - you can get full order details inside onApprove: ', details);
                    this.inscripcion.pago.numeroTransaccion = JSON.stringify(details);
                    this.isPaid = true;
                });
            },
            onClientAuthorization: data => {
                console.log(
                    'onClientAuthorization - you should probably inform your server about completed transaction at this point',
                    data
                );
                // this.showSuccess = true;
            },
            onCancel: (data, actions) => {
                console.log('OnCancel', data, actions);
                // this.showCancel = true;
            },
            onError: err => {
                console.log('OnError', err);
                // this.showError = true;
            },
            onClick: (data, actions) => {
                console.log('onClick', data, actions);
                // this.resetStatus();
            }
        };

        this.accountService.identity().then((account: Account) => {
            this.inscripcion.alumno = account;
        });
        this.activatedRoute.data.subscribe(({ inscripcion }) => {
            this.inscripcion = inscripcion;
            // this.fecha = this.inscripcion.fecha != null ? this.inscripcion.fecha.format(DATE_TIME_FORMAT) : new Date().toString();
        });
        this.activatedRoute.params.subscribe(params => {
            if (params['curso'] && params['curso'] != null) {
                const cursoId = params['curso'];
                this.cursoService
                    .find(cursoId)
                    .pipe(
                        filter((mayBeOk: HttpResponse<ICurso>) => mayBeOk.ok),
                        map((response: HttpResponse<ICurso>) => response.body)
                    )
                    .subscribe(
                        (res: ICurso) => {
                            console.log(res);
                            this.inscripcion.curso = res;
                            this.inscripcion.pago = { tipoPago: res.tipoPagos[0] };
                            // console.log(res.tipoPagos[0]);
                            this.price = res.monto.toString();
                            this.montoOriginal = res.monto;
                            if (this.inscripcion.pago.tipoPago.moneda.id !== this.inscripcion.curso.moneda.id) {
                                this.cambio = {
                                    monto: this.inscripcion.curso.monto * this.inscripcion.pago.tipoPago.moneda.cambio,
                                    moneda: this.inscripcion.pago.tipoPago.moneda
                                };
                            } else {
                                this.cambio = {
                                    monto: this.inscripcion.curso.monto,
                                    moneda: this.inscripcion.curso.moneda
                                };
                            }

                            console.log(res.tipoPagos[0].tipoCuenta);
                            if (res.tipoPagos[0].tipoCuenta.id === 3751) {
                                this.isPaid = true;
                            }

                            this.deseaDonar = true;
                        },
                        (res: HttpErrorResponse) => this.onError(res.message)
                    );
            }
        });

        this.userService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
                map((response: HttpResponse<IUser[]>) => response.body)
            )
            .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        if(((!this.isSaving || this.isPaid) && this.deseaDonar) || (this.inscripcion.curso.monto == 0 && !this.deseaDonar) ) {
            this.isSaving = true;
            this.inscripcion.pagos = [];
            this.inscripcion.pago.moneda = this.cambio.moneda;
            this.inscripcion.pagos.push(this.inscripcion.pago);
            this.inscripcion.fecha = moment(new Date(), DATE_TIME_FORMAT);
            if (this.varios) {
                this.inscripcion.comentarios = 'Pagado para ' + this.cantidadAlumnos + ' alumnos \n' + this.inscripcion.comentarios;
            }
            if (this.inscripcion.id !== undefined) {
                this.subscribeToSaveResponse(this.inscripcionService.update(this.inscripcion));
            } else {
                this.subscribeToSaveResponse(this.inscripcionService.create(this.inscripcion));
            }
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IInscripcion>>) {
        result.subscribe((res: HttpResponse<IInscripcion>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCursoById(index: number, item: ICurso) {
        return item.id;
    }

    trackUserById(index: number, item: IUser) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
        this.isPaid = true;
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.pago, this.elementRef, field, fieldContentType, idInput);
    }

    trackTipoPagoById(index: number, item: ITipoPago) {
        return item.id;
    }

    checkCantidad() {
        if (this.varios) {
            if (this.cantidadAlumnos >= 1) {
                this.inscripcion.curso.monto = this.montoOriginal * this.cantidadAlumnos;
            }
        } else {
            this.inscripcion.curso.monto = this.montoOriginal;
        }
    }

    checkPaypal() {
        if (this.inscripcion.pago && this.inscripcion.pago.tipoPago.tipoCuenta.id === 1) {
            this.paypalId = this.inscripcion.pago.tipoPago.scriptBoton;
            console.log('Same coin: ', this.inscripcion.pago.tipoPago.tipoCuenta, this.inscripcion.curso.moneda);
            if (this.inscripcion.pago.tipoPago.moneda.id !== this.inscripcion.curso.moneda.id) {
                this.cambio = {
                    monto: this.inscripcion.curso.monto * this.inscripcion.pago.tipoPago.moneda.cambio,
                    moneda: this.inscripcion.pago.tipoPago.moneda
                };
            } else {
                this.cambio = {
                    monto: this.inscripcion.curso.monto,
                    moneda: this.inscripcion.curso.moneda
                };
            }
            this.isPaid = false;
        } else if (this.inscripcion.pago && this.inscripcion.pago.tipoPago.tipoCuenta.id === 2) {
            console.log('Same coin: ', this.inscripcion.pago.tipoPago.tipoCuenta, this.inscripcion.curso.moneda);
            if (this.inscripcion.pago.tipoPago.moneda.id !== this.inscripcion.curso.moneda.id) {
                this.cambio = {
                    monto: this.inscripcion.curso.monto * this.inscripcion.pago.tipoPago.moneda.cambio,
                    moneda: this.inscripcion.pago.tipoPago.moneda
                };
            } else {
                this.cambio = {
                    monto: this.inscripcion.curso.monto,
                    moneda: this.inscripcion.curso.moneda
                };
            }
            this.isPaid = false;
        } else if (this.inscripcion.pago && this.inscripcion.pago.tipoPago.tipoCuenta.id === 2601) {
            console.log('Same coin: ', this.inscripcion.pago.tipoPago.tipoCuenta, this.inscripcion.curso.moneda);
            this.cambio = {
                monto: this.inscripcion.curso.monto,
                moneda: this.inscripcion.curso.moneda
            };
            this.isPaid = false;
        } else if (this.inscripcion.pago && this.inscripcion.pago.tipoPago.tipoCuenta.id === 3751) {
            console.log('Same coin: ', this.inscripcion.pago.tipoPago.tipoCuenta, this.inscripcion.curso.moneda);
            this.cambio = {
                monto: this.inscripcion.curso.monto,
                moneda: this.inscripcion.curso.moneda
            };
            this.isPaid = true;
        } else {
            this.isPaid = false;
        }
    }
}
