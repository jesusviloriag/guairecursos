import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';
import { NgxPayPalModule } from 'ngx-paypal';

import { GuaireCursosSharedModule } from 'app/shared';
import {
    InscripcionComponent,
    InscripcionUserComponent,
    InscripcionCursoComponent,
    InscripcionDetailComponent,
    InscripcionUpdateComponent,
    InscripcionEmailComponent,
    InscripcionDeletePopupComponent,
    InscripcionDeleteDialogComponent,
    inscripcionRoute,
    inscripcionPopupRoute
} from './';

const ENTITY_STATES = [...inscripcionRoute, ...inscripcionPopupRoute];

@NgModule({
    imports: [NgxPayPalModule, GuaireCursosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        InscripcionComponent,
        InscripcionUserComponent,
        InscripcionCursoComponent,
        InscripcionDetailComponent,
        InscripcionUpdateComponent,
        InscripcionEmailComponent,
        InscripcionDeleteDialogComponent,
        InscripcionDeletePopupComponent
    ],
    entryComponents: [InscripcionComponent, InscripcionUserComponent, InscripcionCursoComponent, InscripcionUpdateComponent, InscripcionEmailComponent, InscripcionDeleteDialogComponent, InscripcionDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuaireCursosInscripcionModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
