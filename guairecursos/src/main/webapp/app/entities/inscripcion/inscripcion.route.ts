import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Inscripcion } from 'app/shared/model/inscripcion.model';
import { InscripcionService } from './inscripcion.service';
import { InscripcionComponent } from './inscripcion.component';
import { InscripcionUserComponent } from './inscripcion-user.component';
import { InscripcionCursoComponent } from './inscripcion-curso.component';
import { InscripcionDetailComponent } from './inscripcion-detail.component';
import { InscripcionUpdateComponent } from './inscripcion-update.component';
import { InscripcionEmailComponent } from './inscripcion-email.component';
import { InscripcionDeletePopupComponent } from './inscripcion-delete-dialog.component';
import { IInscripcion } from 'app/shared/model/inscripcion.model';

@Injectable({ providedIn: 'root' })
export class InscripcionResolve implements Resolve<IInscripcion> {
    constructor(private service: InscripcionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IInscripcion> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Inscripcion>) => response.ok),
                map((inscripcion: HttpResponse<Inscripcion>) => inscripcion.body)
            );
        }
        return of(new Inscripcion());
    }
}

export const inscripcionRoute: Routes = [
    {
        path: '',
        component: InscripcionComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'user',
        component: InscripcionUserComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'curso/:id',
        component: InscripcionCursoComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: InscripcionDetailComponent,
        resolve: {
            inscripcion: InscripcionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: InscripcionUpdateComponent,
        resolve: {
            inscripcion: InscripcionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new/:curso',
        component: InscripcionUpdateComponent,
        resolve: {
         inscripcion: InscripcionResolve
        },
        data: {
         authorities: ['ROLE_USER'],
         pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'email/:curso',
        component: InscripcionEmailComponent,
        resolve: {
          inscripcion: InscripcionResolve
        },
        data: {
          authorities: ['ROLE_USER'],
          pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const inscripcionPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: InscripcionDeletePopupComponent,
        resolve: {
            inscripcion: InscripcionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.inscripcion.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
