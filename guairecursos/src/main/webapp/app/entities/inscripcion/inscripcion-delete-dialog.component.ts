import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInscripcion } from 'app/shared/model/inscripcion.model';
import { InscripcionService } from './inscripcion.service';

@Component({
    selector: 'jhi-inscripcion-delete-dialog',
    templateUrl: './inscripcion-delete-dialog.component.html'
})
export class InscripcionDeleteDialogComponent {
    inscripcion: IInscripcion;

    constructor(
        protected inscripcionService: InscripcionService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.inscripcionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'inscripcionListModification',
                content: 'Deleted an inscripcion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-inscripcion-delete-popup',
    template: ''
})
export class InscripcionDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ inscripcion }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(InscripcionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.inscripcion = inscripcion;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/inscripcion', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/inscripcion', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
