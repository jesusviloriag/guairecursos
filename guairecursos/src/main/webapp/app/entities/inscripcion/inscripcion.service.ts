import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IInscripcion } from 'app/shared/model/inscripcion.model';

type EntityResponseType = HttpResponse<IInscripcion>;
type EntityArrayResponseType = HttpResponse<IInscripcion[]>;

@Injectable({ providedIn: 'root' })
export class InscripcionService {
    public resourceUrl = SERVER_API_URL + 'api/inscripcions';

    constructor(protected http: HttpClient) {}

    create(inscripcion: IInscripcion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(inscripcion);
        return this.http
            .post<IInscripcion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(inscripcion: IInscripcion): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(inscripcion);
        return this.http
            .put<IInscripcion>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IInscripcion>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IInscripcion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    queryUser(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IInscripcion[]>(this.resourceUrl + '/user', { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    queryCurso(id: number, req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IInscripcion[]>(this.resourceUrl + '/curso/' + `${id}`, { observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    protected convertDateFromClient(inscripcion: IInscripcion): IInscripcion {
        const copy: IInscripcion = Object.assign({}, inscripcion, {
            fecha: inscripcion.fecha != null && inscripcion.fecha.isValid() ? inscripcion.fecha.toJSON() : null
        });
        return copy;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.fecha = res.body.fecha != null ? moment(res.body.fecha) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((inscripcion: IInscripcion) => {
                inscripcion.fecha = inscripcion.fecha != null ? moment(inscripcion.fecha) : null;
            });
        }
        return res;
    }
}
