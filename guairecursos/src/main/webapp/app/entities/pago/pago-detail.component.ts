import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IEstatusPago } from 'app/shared/model/estatus-pago.model';
import { EstatusPagoService } from 'app/entities/estatus-pago';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { PagoService } from './pago.service';

import { IPago } from 'app/shared/model/pago.model';

@Component({
    selector: 'jhi-pago-detail',
    templateUrl: './pago-detail.component.html'
})
export class PagoDetailComponent implements OnInit {
    pago: IPago;

    estatuspagos: IEstatusPago[];

    isSaving: boolean;

    fecha: string;
    fechaStatus: string;

    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute, protected estatusPagoService: EstatusPagoService, protected pagoService: PagoService, protected jhiAlertService: JhiAlertService) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ pago }) => {
            this.pago = pago;
        });
        this.estatusPagoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEstatusPago[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEstatusPago[]>) => response.body)
            )
            .subscribe((res: IEstatusPago[]) => (this.estatuspagos = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.pago.fechaStatus = moment(new Date(), DATE_TIME_FORMAT);
        this.subscribeToSaveResponse(this.pagoService.update(this.pago));
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPago>>) {
        result.subscribe((res: HttpResponse<IPago>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    updatePago() {
        this.save();
    }
}
