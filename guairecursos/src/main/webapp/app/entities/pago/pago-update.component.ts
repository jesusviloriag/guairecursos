import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IPago } from 'app/shared/model/pago.model';
import { PagoService } from './pago.service';
import { ITipoPago } from 'app/shared/model/tipo-pago.model';
import { TipoPagoService } from 'app/entities/tipo-pago';
import { IEstatusPago } from 'app/shared/model/estatus-pago.model';
import { EstatusPagoService } from 'app/entities/estatus-pago';
import { IMoneda } from 'app/shared/model/moneda.model';
import { MonedaService } from 'app/entities/moneda';
import { IInscripcion } from 'app/shared/model/inscripcion.model';
import { InscripcionService } from 'app/entities/inscripcion';
import {
    IPayPalConfig,
    ICreateOrderRequest
} from 'ngx-paypal';

@Component({
    selector: 'jhi-pago-update',
    templateUrl: './pago-update.component.html'
})
export class PagoUpdateComponent implements OnInit {

    public payPalConfig?: IPayPalConfig;

    pago: IPago;
    isSaving: boolean;

    tipopagos: ITipoPago[];

    estatuspagos: IEstatusPago[];

    monedas: IMoneda[];

    inscripcions: IInscripcion[];
    fecha: string;
    fechaStatus: string;

    paypalId: string;
    price: string;

    isPaid: boolean;

    montoOriginal: number;

    cambio: any;

    inscripcion: IInscripcion;

    constructor(
        protected dataUtils: JhiDataUtils,
        protected jhiAlertService: JhiAlertService,
        protected pagoService: PagoService,
        protected tipoPagoService: TipoPagoService,
        protected estatusPagoService: EstatusPagoService,
        protected monedaService: MonedaService,
        protected inscripcionService: InscripcionService,
        protected elementRef: ElementRef,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.paypalId = 'AcnlYadc2ABqHZvnOWgGz1A74bd0BNFkJtZsL083Y816fNz3A8M9M4Ql205s4EVdzLl5XIv2xpbTUOMm';

        this.payPalConfig = {
            currency: 'USD',
            clientId: this.paypalId,
            createOrderOnClient: data =>
                <ICreateOrderRequest>{
                    intent: 'CAPTURE',
                    purchase_units: [
                        {
                            amount: {
                                value: this.price.toString()
                            }
                        }
                    ]
                },
            advanced: {
                commit: 'true'
            },
            style: {
                label: 'paypal',
                layout: 'vertical'
            },
            onApprove: (data, actions) => {
                console.log('onApprove - transaction was approved, but not authorized', data, actions);
                alert('Transacción exitosa');
                actions.order.get().then(details => {
                    console.log('onApprove - you can get full order details inside onApprove: ', details);
                    this.pago.numeroTransaccion = JSON.stringify(details);
                    this.isPaid = true;
                });
            },
            onClientAuthorization: data => {
                console.log(
                    'onClientAuthorization - you should probably inform your server about completed transaction at this point',
                    data
                );
                // this.showSuccess = true;
            },
            onCancel: (data, actions) => {
                console.log('OnCancel', data, actions);
                // this.showCancel = true;
            },
            onError: err => {
                console.log('OnError', err);
                // this.showError = true;
            },
            onClick: (data, actions) => {
                console.log('onClick', data, actions);
                // this.resetStatus();
            }
        };

        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ pago }) => {
            this.pago = pago;
            const current_datetime = new Date();
            this.fecha =
                this.pago.fecha != null
                    ? this.pago.fecha.format(DATE_TIME_FORMAT)
                    : current_datetime.getFullYear() + '-' + (current_datetime.getMonth() + 1) + '-' + current_datetime.getDate();
            this.fechaStatus = this.pago.fechaStatus != null ? this.pago.fechaStatus.format(DATE_TIME_FORMAT) : null;
        });
        this.activatedRoute.params.subscribe(params => {
            if (params['inscripcion'] && params['inscripcion'] != null) {
                const inscripcionId = params['inscripcion'];
                this.inscripcionService
                    .find(inscripcionId)
                    .pipe(
                        filter((mayBeOk: HttpResponse<IInscripcion>) => mayBeOk.ok),
                        map((response: HttpResponse<IInscripcion>) => response.body)
                    )
                    .subscribe(
                        (res: IInscripcion) => {
                            console.log(res);
                            this.pago.inscripcion = res;
                            this.pago.tipoPago = res.curso.tipoPagos[0];
                            // console.log(res.tipoPagos[0]);
                            this.price = res.curso.monto.toString();
                            this.montoOriginal = res.curso.monto;
                            if (this.pago.tipoPago.moneda.id !== this.pago.inscripcion.curso.moneda.id) {
                                this.cambio = {
                                    monto: this.pago.inscripcion.curso.monto * this.pago.tipoPago.moneda.cambio,
                                    moneda: this.pago.tipoPago.moneda
                                };
                            } else {
                                this.cambio = {
                                    monto: this.pago.inscripcion.curso.monto,
                                    moneda: this.pago.inscripcion.curso.moneda
                                };
                            }

                            console.log(res.curso.tipoPagos[0].tipoCuenta);
                            if (res.curso.tipoPagos[0].tipoCuenta.id === 3751) {
                                this.isPaid = true;
                            }
                        },
                        (res: HttpErrorResponse) => this.onError(res.message)
                    );
            }
        });
        this.tipoPagoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ITipoPago[]>) => mayBeOk.ok),
                map((response: HttpResponse<ITipoPago[]>) => response.body)
            )
            .subscribe((res: ITipoPago[]) => (this.tipopagos = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.estatusPagoService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IEstatusPago[]>) => mayBeOk.ok),
                map((response: HttpResponse<IEstatusPago[]>) => response.body)
            )
            .subscribe((res: IEstatusPago[]) => (this.estatuspagos = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.monedaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMoneda[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMoneda[]>) => response.body)
            )
            .subscribe((res: IMoneda[]) => (this.monedas = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.inscripcionService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IInscripcion[]>) => mayBeOk.ok),
                map((response: HttpResponse<IInscripcion[]>) => response.body)
            )
            .subscribe((res: IInscripcion[]) => (this.inscripcions = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clearInputImage(field: string, fieldContentType: string, idInput: string) {
        this.dataUtils.clearInputImage(this.pago, this.elementRef, field, fieldContentType, idInput);
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.pago.fecha = this.fecha != null ? moment(this.fecha, DATE_TIME_FORMAT) : null;
        this.pago.fechaStatus = this.fechaStatus != null ? moment(this.fechaStatus, DATE_TIME_FORMAT) : null;
        this.pago.moneda = this.cambio.moneda;
        if (this.pago.id !== undefined) {
            this.subscribeToSaveResponse(this.pagoService.update(this.pago));
        } else {
            this.subscribeToSaveResponse(this.pagoService.create(this.pago));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IPago>>) {
        result.subscribe((res: HttpResponse<IPago>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTipoPagoById(index: number, item: ITipoPago) {
        return item.id;
    }

    trackEstatusPagoById(index: number, item: IEstatusPago) {
        return item.id;
    }

    trackMonedaById(index: number, item: IMoneda) {
        return item.id;
    }

    trackInscripcionById(index: number, item: IInscripcion) {
        return item.id;
    }

    checkPaypal() {
        console.log(this.pago);
        if (this.pago && this.pago.tipoPago.tipoCuenta.id === 1) {
            this.paypalId = this.pago.tipoPago.scriptBoton;
            console.log('Same coin: ', this.pago.tipoPago.tipoCuenta, this.pago.inscripcion.curso.moneda);
            if (this.pago.tipoPago.moneda.id !== this.pago.inscripcion.curso.moneda.id) {
                this.cambio = {
                    monto: this.pago.inscripcion.curso.monto * this.pago.tipoPago.moneda.cambio,
                    moneda: this.pago.tipoPago.moneda
                };
            } else {
                this.cambio = {
                      monto: this.pago.inscripcion.curso.monto,
                      moneda: this.pago.tipoPago.moneda
                  };
            }
            this.isPaid = false;
        } else if (this.pago && this.pago.tipoPago.tipoCuenta.id === 2) {
            console.log('Same coin: ', this.pago.tipoPago.tipoCuenta, this.pago.inscripcion.curso.moneda);
            if (this.pago.tipoPago.moneda.id !== this.pago.inscripcion.curso.moneda.id) {
                this.cambio = {
                    monto: this.pago.inscripcion.curso.monto * this.pago.tipoPago.moneda.cambio,
                    moneda: this.pago.tipoPago.moneda
                };
            } else {
                this.cambio = {
                      monto: this.pago.inscripcion.curso.monto,
                      moneda: this.pago.tipoPago.moneda
                  };
            }
            this.isPaid = false;
        } else if (this.pago && this.pago.tipoPago.tipoCuenta.id === 2601) {
            console.log('Same coin: ', this.pago.tipoPago.tipoCuenta, this.pago.inscripcion.curso.moneda);
            this.cambio = {
                  monto: this.pago.inscripcion.curso.monto,
                  moneda: this.pago.tipoPago.moneda
              };
            this.isPaid = false;
        } else if (this.pago && this.pago.tipoPago.tipoCuenta.id === 3751) {
            console.log('Same coin: ', this.pago.tipoPago.tipoCuenta, this.pago.inscripcion.curso.moneda);
            this.cambio = {
                  monto: this.pago.inscripcion.curso.monto,
                  moneda: this.pago.tipoPago.moneda
              };
            this.isPaid = true;
        } else {
            this.isPaid = false;
        }
    }
}
