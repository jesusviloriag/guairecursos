import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IMoneda } from 'app/shared/model/moneda.model';
import { MonedaService } from './moneda.service';

@Component({
    selector: 'jhi-moneda-update',
    templateUrl: './moneda-update.component.html'
})
export class MonedaUpdateComponent implements OnInit {
    moneda: IMoneda;
    isSaving: boolean;

    constructor(protected monedaService: MonedaService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ moneda }) => {
            this.moneda = moneda;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.moneda.id !== undefined) {
            this.subscribeToSaveResponse(this.monedaService.update(this.moneda));
        } else {
            this.subscribeToSaveResponse(this.monedaService.create(this.moneda));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IMoneda>>) {
        result.subscribe((res: HttpResponse<IMoneda>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
