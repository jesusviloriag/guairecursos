import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Moneda } from 'app/shared/model/moneda.model';
import { MonedaService } from './moneda.service';
import { MonedaComponent } from './moneda.component';
import { MonedaDetailComponent } from './moneda-detail.component';
import { MonedaUpdateComponent } from './moneda-update.component';
import { MonedaDeletePopupComponent } from './moneda-delete-dialog.component';
import { IMoneda } from 'app/shared/model/moneda.model';

@Injectable({ providedIn: 'root' })
export class MonedaResolve implements Resolve<IMoneda> {
    constructor(private service: MonedaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMoneda> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Moneda>) => response.ok),
                map((moneda: HttpResponse<Moneda>) => moneda.body)
            );
        }
        return of(new Moneda());
    }
}

export const monedaRoute: Routes = [
    {
        path: '',
        component: MonedaComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'guaireCursosApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: MonedaDetailComponent,
        resolve: {
            moneda: MonedaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: MonedaUpdateComponent,
        resolve: {
            moneda: MonedaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: MonedaUpdateComponent,
        resolve: {
            moneda: MonedaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const monedaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: MonedaDeletePopupComponent,
        resolve: {
            moneda: MonedaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.moneda.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
