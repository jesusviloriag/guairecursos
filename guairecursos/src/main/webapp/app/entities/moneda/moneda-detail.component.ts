import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMoneda } from 'app/shared/model/moneda.model';

@Component({
    selector: 'jhi-moneda-detail',
    templateUrl: './moneda-detail.component.html'
})
export class MonedaDetailComponent implements OnInit {
    moneda: IMoneda;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ moneda }) => {
            this.moneda = moneda;
        });
    }

    previousState() {
        window.history.back();
    }
}
