import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { GuaireCursosSharedModule } from 'app/shared';
import {
    MonedaComponent,
    MonedaDetailComponent,
    MonedaUpdateComponent,
    MonedaDeletePopupComponent,
    MonedaDeleteDialogComponent,
    monedaRoute,
    monedaPopupRoute
} from './';

const ENTITY_STATES = [...monedaRoute, ...monedaPopupRoute];

@NgModule({
    imports: [GuaireCursosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [MonedaComponent, MonedaDetailComponent, MonedaUpdateComponent, MonedaDeleteDialogComponent, MonedaDeletePopupComponent],
    entryComponents: [MonedaComponent, MonedaUpdateComponent, MonedaDeleteDialogComponent, MonedaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuaireCursosMonedaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
