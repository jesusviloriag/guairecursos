import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ITipoCuenta } from 'app/shared/model/tipo-cuenta.model';

type EntityResponseType = HttpResponse<ITipoCuenta>;
type EntityArrayResponseType = HttpResponse<ITipoCuenta[]>;

@Injectable({ providedIn: 'root' })
export class TipoCuentaService {
    public resourceUrl = SERVER_API_URL + 'api/tipo-cuentas';

    constructor(protected http: HttpClient) {}

    create(tipoCuenta: ITipoCuenta): Observable<EntityResponseType> {
        return this.http.post<ITipoCuenta>(this.resourceUrl, tipoCuenta, { observe: 'response' });
    }

    update(tipoCuenta: ITipoCuenta): Observable<EntityResponseType> {
        return this.http.put<ITipoCuenta>(this.resourceUrl, tipoCuenta, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ITipoCuenta>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ITipoCuenta[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
