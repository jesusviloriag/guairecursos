import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITipoCuenta } from 'app/shared/model/tipo-cuenta.model';

@Component({
    selector: 'jhi-tipo-cuenta-detail',
    templateUrl: './tipo-cuenta-detail.component.html'
})
export class TipoCuentaDetailComponent implements OnInit {
    tipoCuenta: ITipoCuenta;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ tipoCuenta }) => {
            this.tipoCuenta = tipoCuenta;
        });
    }

    previousState() {
        window.history.back();
    }
}
