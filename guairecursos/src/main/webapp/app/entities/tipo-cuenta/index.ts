export * from './tipo-cuenta.service';
export * from './tipo-cuenta-update.component';
export * from './tipo-cuenta-delete-dialog.component';
export * from './tipo-cuenta-detail.component';
export * from './tipo-cuenta.component';
export * from './tipo-cuenta.route';
