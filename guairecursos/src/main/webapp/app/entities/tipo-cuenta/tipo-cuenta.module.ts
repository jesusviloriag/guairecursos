import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { GuaireCursosSharedModule } from 'app/shared';
import {
    TipoCuentaComponent,
    TipoCuentaDetailComponent,
    TipoCuentaUpdateComponent,
    TipoCuentaDeletePopupComponent,
    TipoCuentaDeleteDialogComponent,
    tipoCuentaRoute,
    tipoCuentaPopupRoute
} from './';

const ENTITY_STATES = [...tipoCuentaRoute, ...tipoCuentaPopupRoute];

@NgModule({
    imports: [GuaireCursosSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        TipoCuentaComponent,
        TipoCuentaDetailComponent,
        TipoCuentaUpdateComponent,
        TipoCuentaDeleteDialogComponent,
        TipoCuentaDeletePopupComponent
    ],
    entryComponents: [TipoCuentaComponent, TipoCuentaUpdateComponent, TipoCuentaDeleteDialogComponent, TipoCuentaDeletePopupComponent],
    providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuaireCursosTipoCuentaModule {
    constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
        this.languageHelper.language.subscribe((languageKey: string) => {
            if (languageKey !== undefined) {
                this.languageService.changeLanguage(languageKey);
            }
        });
    }
}
