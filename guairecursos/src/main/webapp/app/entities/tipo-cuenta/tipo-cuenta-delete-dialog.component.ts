import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITipoCuenta } from 'app/shared/model/tipo-cuenta.model';
import { TipoCuentaService } from './tipo-cuenta.service';

@Component({
    selector: 'jhi-tipo-cuenta-delete-dialog',
    templateUrl: './tipo-cuenta-delete-dialog.component.html'
})
export class TipoCuentaDeleteDialogComponent {
    tipoCuenta: ITipoCuenta;

    constructor(
        protected tipoCuentaService: TipoCuentaService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tipoCuentaService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'tipoCuentaListModification',
                content: 'Deleted an tipoCuenta'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tipo-cuenta-delete-popup',
    template: ''
})
export class TipoCuentaDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ tipoCuenta }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(TipoCuentaDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.tipoCuenta = tipoCuenta;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/tipo-cuenta', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/tipo-cuenta', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
