import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ITipoCuenta } from 'app/shared/model/tipo-cuenta.model';
import { TipoCuentaService } from './tipo-cuenta.service';

@Component({
    selector: 'jhi-tipo-cuenta-update',
    templateUrl: './tipo-cuenta-update.component.html'
})
export class TipoCuentaUpdateComponent implements OnInit {
    tipoCuenta: ITipoCuenta;
    isSaving: boolean;

    constructor(protected tipoCuentaService: TipoCuentaService, protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ tipoCuenta }) => {
            this.tipoCuenta = tipoCuenta;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.tipoCuenta.id !== undefined) {
            this.subscribeToSaveResponse(this.tipoCuentaService.update(this.tipoCuenta));
        } else {
            this.subscribeToSaveResponse(this.tipoCuentaService.create(this.tipoCuenta));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITipoCuenta>>) {
        result.subscribe((res: HttpResponse<ITipoCuenta>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
