import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { TipoCuenta } from 'app/shared/model/tipo-cuenta.model';
import { TipoCuentaService } from './tipo-cuenta.service';
import { TipoCuentaComponent } from './tipo-cuenta.component';
import { TipoCuentaDetailComponent } from './tipo-cuenta-detail.component';
import { TipoCuentaUpdateComponent } from './tipo-cuenta-update.component';
import { TipoCuentaDeletePopupComponent } from './tipo-cuenta-delete-dialog.component';
import { ITipoCuenta } from 'app/shared/model/tipo-cuenta.model';

@Injectable({ providedIn: 'root' })
export class TipoCuentaResolve implements Resolve<ITipoCuenta> {
    constructor(private service: TipoCuentaService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITipoCuenta> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<TipoCuenta>) => response.ok),
                map((tipoCuenta: HttpResponse<TipoCuenta>) => tipoCuenta.body)
            );
        }
        return of(new TipoCuenta());
    }
}

export const tipoCuentaRoute: Routes = [
    {
        path: '',
        component: TipoCuentaComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'guaireCursosApp.tipoCuenta.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: TipoCuentaDetailComponent,
        resolve: {
            tipoCuenta: TipoCuentaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.tipoCuenta.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: TipoCuentaUpdateComponent,
        resolve: {
            tipoCuenta: TipoCuentaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.tipoCuenta.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: TipoCuentaUpdateComponent,
        resolve: {
            tipoCuenta: TipoCuentaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.tipoCuenta.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tipoCuentaPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: TipoCuentaDeletePopupComponent,
        resolve: {
            tipoCuenta: TipoCuentaResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'guaireCursosApp.tipoCuenta.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
