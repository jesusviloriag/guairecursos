import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ITipoPago } from 'app/shared/model/tipo-pago.model';
import { TipoPagoService } from './tipo-pago.service';
import { IMoneda } from 'app/shared/model/moneda.model';
import { MonedaService } from 'app/entities/moneda';
import { ITipoCuenta } from 'app/shared/model/tipo-cuenta.model';
import { TipoCuentaService } from 'app/entities/tipo-cuenta';

@Component({
    selector: 'jhi-tipo-pago-update',
    templateUrl: './tipo-pago-update.component.html'
})
export class TipoPagoUpdateComponent implements OnInit {
    tipoPago: ITipoPago;
    isSaving: boolean;

    monedas: IMoneda[];

    tipocuentas: ITipoCuenta[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected tipoPagoService: TipoPagoService,
        protected monedaService: MonedaService,
        protected tipoCuentaService: TipoCuentaService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ tipoPago }) => {
            this.tipoPago = tipoPago;
            this.tipoPago.instantaneo = false;
        });
        this.monedaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IMoneda[]>) => mayBeOk.ok),
                map((response: HttpResponse<IMoneda[]>) => response.body)
            )
            .subscribe((res: IMoneda[]) => (this.monedas = res), (res: HttpErrorResponse) => this.onError(res.message));
        this.tipoCuentaService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<ITipoCuenta[]>) => mayBeOk.ok),
                map((response: HttpResponse<ITipoCuenta[]>) => response.body)
            )
            .subscribe((res: ITipoCuenta[]) => (this.tipocuentas = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.tipoPago.id !== undefined) {
            this.subscribeToSaveResponse(this.tipoPagoService.update(this.tipoPago));
        } else {
            this.subscribeToSaveResponse(this.tipoPagoService.create(this.tipoPago));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ITipoPago>>) {
        result.subscribe((res: HttpResponse<ITipoPago>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackMonedaById(index: number, item: IMoneda) {
        return item.id;
    }

    trackTipoCuentaById(index: number, item: ITipoCuenta) {
        return item.id;
    }
}
