export interface IMoneda {
    id?: number;
    nombre?: string;
    simbolo?: string;
    cambio?: number;
}

export class Moneda implements IMoneda {
    constructor(public id?: number, public nombre?: string, public simbolo?: string, public cambio?: number) {}
}
