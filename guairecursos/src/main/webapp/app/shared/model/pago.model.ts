import { Moment } from 'moment';
import { ITipoPago } from 'app/shared/model/tipo-pago.model';
import { IEstatusPago } from 'app/shared/model/estatus-pago.model';
import { IMoneda } from 'app/shared/model/moneda.model';
import { IInscripcion } from 'app/shared/model/inscripcion.model';

export interface IPago {
    id?: number;
    fecha?: Moment;
    monto?: number;
    fotoComprobanteContentType?: string;
    fotoComprobante?: any;
    fechaStatus?: Moment;
    numeroTransaccion?: string;
    tipoPago?: ITipoPago;
    estatusPago?: IEstatusPago;
    moneda?: IMoneda;
    inscripcion?: IInscripcion;
}

export class Pago implements IPago {
    constructor(
        public id?: number,
        public fecha?: Moment,
        public monto?: number,
        public fotoComprobanteContentType?: string,
        public fotoComprobante?: any,
        public fechaStatus?: Moment,
        public numeroTransaccion?: string,
        public tipoPago?: ITipoPago,
        public estatusPago?: IEstatusPago,
        public moneda?: IMoneda,
        public inscripcion?: IInscripcion
    ) {}
}
