import { IMoneda } from 'app/shared/model/moneda.model';
import { ITipoCuenta } from 'app/shared/model/tipo-cuenta.model';

export interface ITipoPago {
    id?: number;
    nombre?: string;
    instantaneo?: boolean;
    cuenta?: string;
    email?: string;
    cedula?: string;
    titular?: string;
    scriptBoton?: string;
    moneda?: IMoneda;
    tipoCuenta?: ITipoCuenta;
}

export class TipoPago implements ITipoPago {
    constructor(
        public id?: number,
        public nombre?: string,
        public instantaneo?: boolean,
        public cuenta?: string,
        public email?: string,
        public cedula?: string,
        public titular?: string,
        public scriptBoton?: string,
        public moneda?: IMoneda,
        public tipoCuenta?: ITipoCuenta
    ) {
        this.instantaneo = this.instantaneo || false;
    }
}
