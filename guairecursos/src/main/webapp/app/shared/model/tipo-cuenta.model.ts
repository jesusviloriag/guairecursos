export interface ITipoCuenta {
    id?: number;
    nombre?: string;
    descripcion?: string;
    instantaneo?: boolean;
}

export class TipoCuenta implements ITipoCuenta {
    constructor(public id?: number, public nombre?: string, public descripcion?: string, public instantaneo?: boolean) {
        this.instantaneo = this.instantaneo || false;
    }
}
