import { ICurso } from 'app/shared/model/curso.model';

export interface IGrupo {
    id?: number;
    nombre?: string;
    orden?: string;
    cursos?: ICurso[];
}

export class Grupo implements IGrupo {
    constructor(public id?: number, public nombre?: string, public orden?: string, public cursos?: ICurso[]) {}
}
