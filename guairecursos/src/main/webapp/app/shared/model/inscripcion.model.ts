import { Moment } from 'moment';
import { ICurso } from 'app/shared/model/curso.model';
import { IUser } from 'app/core/user/user.model';
import { IPago } from 'app/shared/model/pago.model';

export interface IInscripcion {
    id?: number;
    fecha?: Moment;
    comentarios?: string;
    curso?: ICurso;
    alumno?: IUser;
    pagos?: IPago[];
    pago?: IPago;
}

export class Inscripcion implements IInscripcion {
    constructor(
        public id?: number,
        public fecha?: Moment,
        public comentarios?: string,
        public curso?: ICurso,
        public alumno?: IUser,
        public pagos?: IPago[],
        public pago?: IPago
    ) {}
}
