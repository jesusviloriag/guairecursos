import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IMoneda } from 'app/shared/model/moneda.model';
import { ITipoPago } from 'app/shared/model/tipo-pago.model';

export interface ICurso {
    id?: number;
    nombre?: string;
    descripcion?: string;
    lugar?: string;
    imagenContentType?: string;
    imagen?: any;
    fecha?: Moment;
    horaInicio?: number;
    minutoInicio?: number;
    horaFin?: number;
    minutoFin?: number;
    linkInformativo?: string;
    monto?: number;
    expositor?: IUser;
    moneda?: IMoneda;
    tipoPagos?: ITipoPago[];
    cupos?: number;
}

export class Curso implements ICurso {
    constructor(
        public id?: number,
        public nombre?: string,
        public descripcion?: string,
        public lugar?: string,
        public imagenContentType?: string,
        public imagen?: any,
        public fecha?: Moment,
        public horaInicio?: number,
        public minutoInicio?: number,
        public horaFin?: number,
        public minutoFin?: number,
        public linkInformativo?: string,
        public monto?: number,
        public expositor?: IUser,
        public moneda?: IMoneda,
        public tipoPagos?: ITipoPago[],
        public cupos?: number,
    ) {}
}
