import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';

import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { ICurso } from 'app/shared/model/curso.model';

import { ITEMS_PER_PAGE } from 'app/shared';
import { CursoService } from '../entities/curso/curso.service';

import { LoginModalService, AccountService, Account } from 'app/core';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;

    currentAccount: any;
    cursos: ICurso[];
    curso: ICurso;
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    fechaInvalida: boolean;

    constructor(
        private accountService: AccountService,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        protected cursoService: CursoService,
        protected activatedRoute: ActivatedRoute,
        protected dataUtils: JhiDataUtils,
        protected router: Router,
        protected parseLinks: JhiParseLinks,
        protected jhiAlertService: JhiAlertService
    ) {
        this.itemsPerPage = 1;
        this.page = 0;
    }

    loadAll() {
        this.cursoService
            .query({
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            })
            .subscribe(
                (res: HttpResponse<ICurso[]>) => this.paginateCursos(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then((account: Account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', message => {
            this.accountService.identity().then(account => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.accountService.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    registerChangeInCursos() {
        this.eventSubscriber = this.eventManager.subscribe('cursoListModification', response => this.loadAll());
    }

    sort() {
        const result = ['id,desc'];
        return result;
    }

    protected paginateCursos(data: ICurso[], headers: HttpHeaders) {
        if (data && data.length > 0) {
            this.curso = data[0];
            console.log(this.curso);
            if (this.curso.fecha.toDate().getTime() < new Date().getTime()) {
                this.fechaInvalida = true;
            } else {
                this.fechaInvalida = false;
            }
        }
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    calculateFate(a) {
        console.log(a);
        if (a) {
            console.log(a);
            if (a.toDate().getTime() < new Date().getTime()) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
