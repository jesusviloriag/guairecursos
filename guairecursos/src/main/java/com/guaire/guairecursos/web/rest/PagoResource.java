package com.guaire.guairecursos.web.rest;
import com.guaire.guairecursos.domain.Pago;
import com.guaire.guairecursos.domain.User;
import com.guaire.guairecursos.repository.EstatusPagoRepository;
import com.guaire.guairecursos.repository.PagoRepository;
import com.guaire.guairecursos.web.rest.errors.BadRequestAlertException;
import com.guaire.guairecursos.web.rest.util.HeaderUtil;
import com.guaire.guairecursos.web.rest.util.PaginationUtil;
import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Pago.
 */
@RestController
@RequestMapping("/api")
public class PagoResource {

    private final Logger log = LoggerFactory.getLogger(PagoResource.class);

    private static final String ENTITY_NAME = "pago";

    private final PagoRepository pagoRepository;

    private final EstatusPagoRepository estatusPagoRepository;

    private JavaMailSenderImpl javaMailSender;

    private JHipsterProperties jHipsterProperties;

    public PagoResource(PagoRepository pagoRepository, EstatusPagoRepository estatusPagoRepository, JavaMailSenderImpl javaMailSender, JHipsterProperties jHipsterProperties) {
        this.pagoRepository = pagoRepository;
        this.estatusPagoRepository = estatusPagoRepository;
        this.javaMailSender = javaMailSender;
        this.jHipsterProperties = jHipsterProperties;
    }

    /**
     * POST  /pagos : Create a new pago.
     *
     * @param pago the pago to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pago, or with status 400 (Bad Request) if the pago has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pagos")
    public ResponseEntity<Pago> createPago(@Valid @RequestBody Pago pago) throws URISyntaxException, MessagingException {
        log.debug("REST request to save Pago : {}", pago);
        if (pago.getId() != null) {
            throw new BadRequestAlertException("A new pago cannot already have an ID", ENTITY_NAME, "idexists");
        }

        pago.setFecha(Instant.now());
        if(pago.getTipoPago().getTipoCuenta().getId() == 3751L){
            pago.setEstatusPago(estatusPagoRepository.findById(3L).get());
        }if(pago.getTipoPago().getTipoCuenta().getId() == 2601L){
            pago.setEstatusPago(estatusPagoRepository.findById(1L).get());
        }else if (pago.getTipoPago().getTipoCuenta().getId() == 2L){
            pago.setEstatusPago(estatusPagoRepository.findById(1L).get());
        } else if (pago.getTipoPago().getTipoCuenta().getId() == 1L){
            pago.setEstatusPago(estatusPagoRepository.findById(2L).get());
        }

        Pago result = pagoRepository.save(pago);

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, CharEncoding.UTF_8);
        message.setTo(pago.getInscripcion().getAlumno().getEmail());
        message.setFrom(jHipsterProperties.getMail().getFrom());
        message.setSubject("\"Pago para el Curso " + pago.getInscripcion().getCurso().getNombre() );
        message.setText("Estimado(a) " + pago.getInscripcion().getAlumno().getLogin() + ",<br/><br/>El pago para el Curso: <b>" + pago.getInscripcion().getCurso().getNombre() + "</b>" +
            " ha sido registrado." +
            "<br/><br/>Tipo de pago: " + pago.getTipoPago().getNombre() +
            "<br/>Monto: " + pago.getMonto() + pago.getMoneda().getSimbolo() +
            "<br/><br/>Puede ingresar en <a href=\"https://guaire.net/cursos/?inscripcion/user\">guaire.net</a> para verificar su estatus<br/>" +
            "<br/><br/>Atentamente,<br/>El equipo de Guaire Cursos", true);
        javaMailSender.send(mimeMessage);

        mimeMessage = javaMailSender.createMimeMessage();
        message = new MimeMessageHelper(mimeMessage, true, CharEncoding.UTF_8);
        User user = pago.getInscripcion().getCurso().getExpositor();
        message.setTo(user.getEmail());
        message.setFrom(jHipsterProperties.getMail().getFrom());
        message.setSubject("\"Inscripción exitosa para el Curso " + pago.getInscripcion().getCurso().getNombre() );
        message.setText("Estimado(a) " + user.getLogin() + ",<br/><br/>La inscripción para el Curso: <b>" + pago.getInscripcion().getCurso().getNombre() + "</b>" +
            " por parte del usuario " + pago.getInscripcion().getAlumno().getEmail() + " ha sido exitosa." +
            "<br/><br/>Tipo de pago: " + pago.getTipoPago().getNombre() +
            "<br/>Monto: " + pago.getMonto() + pago.getMoneda().getSimbolo() +
            "<br/><br/>Atentamente,<br/>El equipo de Guaire Cursos", true);

        javaMailSender.send(mimeMessage);

        return ResponseEntity.created(new URI("/api/pagos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pagos : Updates an existing pago.
     *
     * @param pago the pago to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pago,
     * or with status 400 (Bad Request) if the pago is not valid,
     * or with status 500 (Internal Server Error) if the pago couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pagos")
    public ResponseEntity<Pago> updatePago(@Valid @RequestBody Pago pago) throws URISyntaxException {
        log.debug("REST request to update Pago : {}", pago);
        if (pago.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Pago result = pagoRepository.save(pago);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pago.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pagos : get all the pagos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pagos in body
     */
    @GetMapping("/pagos")
    public ResponseEntity<List<Pago>> getAllPagos(Pageable pageable) {
        log.debug("REST request to get a page of Pagos");
        Page<Pago> page = pagoRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pagos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /pagos/:id : get the "id" pago.
     *
     * @param id the id of the pago to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pago, or with status 404 (Not Found)
     */
    @GetMapping("/pagos/{id}")
    public ResponseEntity<Pago> getPago(@PathVariable Long id) {
        log.debug("REST request to get Pago : {}", id);
        Optional<Pago> pago = pagoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(pago);
    }

    /**
     * DELETE  /pagos/:id : delete the "id" pago.
     *
     * @param id the id of the pago to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pagos/{id}")
    public ResponseEntity<Void> deletePago(@PathVariable Long id) {
        log.debug("REST request to delete Pago : {}", id);
        pagoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
