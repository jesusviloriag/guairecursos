package com.guaire.guairecursos.web.rest;
import com.guaire.guairecursos.domain.Authority;
import com.guaire.guairecursos.domain.Inscripcion;
import com.guaire.guairecursos.domain.Pago;
import com.guaire.guairecursos.domain.User;
import com.guaire.guairecursos.repository.*;
import com.guaire.guairecursos.security.AuthoritiesConstants;
import com.guaire.guairecursos.service.MailService;
import com.guaire.guairecursos.service.UserService;
import com.guaire.guairecursos.service.dto.UserDTO;
import com.guaire.guairecursos.web.rest.errors.BadRequestAlertException;
import com.guaire.guairecursos.web.rest.util.HeaderUtil;
import com.guaire.guairecursos.web.rest.util.PaginationUtil;
import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.security.Principal;
import java.time.Instant;
import java.util.*;

/**
 * REST controller for managing Inscripcion.
 */
@RestController
@RequestMapping("/api")
public class InscripcionResource {

    private final Logger log = LoggerFactory.getLogger(InscripcionResource.class);

    private static final String ENTITY_NAME = "inscripcion";

    private final InscripcionRepository inscripcionRepository;

    private final UserService userService;

    private final UserRepository userRepository;

    private final MailService mailService;

    private final PagoRepository pagoRepository;

    private final EstatusPagoRepository estatusPagoRepository;

    private JHipsterProperties jHipsterProperties;

    private JavaMailSenderImpl javaMailSender;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public InscripcionResource(InscripcionRepository inscripcionRepository, PagoRepository pagoRepository, UserService userService,
                               UserRepository userRepository, MailService mailService, EstatusPagoRepository estatusPagoRepository,
                               JavaMailSenderImpl javaMailSender, JHipsterProperties jHipsterProperties, PasswordEncoder passwordEncoder,
                               AuthorityRepository authorityRepository) {
        this.inscripcionRepository = inscripcionRepository;
        this.pagoRepository = pagoRepository;
        this.userService = userService;
        this.userRepository = userRepository;
        this.mailService = mailService;
        this.estatusPagoRepository = estatusPagoRepository;
        this.javaMailSender = javaMailSender;
        this.jHipsterProperties = jHipsterProperties;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    /**
     * POST  /inscripcions : Create a new inscripcion.
     *
     * @param inscripcion the inscripcion to create
     * @return the ResponseEntity with status 201 (Created) and with body the new inscripcion, or with status 400 (Bad Request) if the inscripcion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/inscripcions")
    public ResponseEntity<Inscripcion> createInscripcion(@Valid @RequestBody Inscripcion inscripcion, Principal principal) throws URISyntaxException, MessagingException {
        log.debug("REST request to save Inscripcion : {}", inscripcion);
        if (inscripcion.getId() != null) {
            throw new BadRequestAlertException("A new inscripcion cannot already have an ID", ENTITY_NAME, "idexists");
        }

        Inscripcion result = null;

        if(inscripcion.getAlumno().getId() == null){

            try {
                User userFound = userRepository.findOneByEmailIgnoreCase(inscripcion.getAlumno().getEmail()).get();

                log.error("INFO saving inscripcion - user found: " + userFound.getLogin());

                inscripcion.setAlumno(userFound);
                result = inscripcionRepository.save(inscripcion);

                log.error("INFO saving inscripcion - inscripcion saved: " + result.getId());

                MimeMessage mimeMessage = javaMailSender.createMimeMessage();
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, CharEncoding.UTF_8);
                message.setTo(userFound.getEmail());
                message.setFrom(jHipsterProperties.getMail().getFrom());
                message.setSubject("\"Inscripción exitosa para el Curso " + inscripcion.getCurso().getNombre() );
                message.setText("Estimado(a) " + userFound.getLogin() + ",<br/><br/>La inscripción para el Curso: <b>" + inscripcion.getCurso().getNombre() + "</b>" +
                    " ha sido exitosa. Le enviaremos un correo de notificación el día anterior al Curso." +
                    "<br/><br/>Puede ingresar en <a href=\"https://guaire.net/cursos/?inscripcion/user\">guaire.net</a>" +
                    "<br/><br/>Atentamente,<br/>El equipo de Guaire Cursos", true);

                javaMailSender.send(mimeMessage);

                log.error("INFO saving inscripcion - email sent to user successfully");

            } catch(Exception ex){
                log.error("ERROR saving inscripcion: " + ex.getMessage() + " " + ex.toString());

                UserDTO userDTO = new UserDTO();
                userDTO.setEmail(inscripcion.getAlumno().getEmail());
                userDTO.setLogin(inscripcion.getAlumno().getEmail());
                User newUser = userService.createUser(userDTO);
                //mailService.sendCreationEmail(newUser);

                Random r = new Random();
                int randNum = r.nextInt((999 - 100) + 1) + 100;
                String newPassword = Integer.toString(randNum);
                newUser.setPassword(passwordEncoder.encode(newPassword));
                Set<Authority> authorities = new HashSet<>();
                authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
                newUser.setAuthorities(authorities);
                userRepository.save(newUser);

                inscripcion.setAlumno(newUser);
                result = inscripcionRepository.save(inscripcion);

                MimeMessage mimeMessage = javaMailSender.createMimeMessage();
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, CharEncoding.UTF_8);
                message.setTo(newUser.getEmail());
                message.setFrom(jHipsterProperties.getMail().getFrom());
                message.setSubject("\"Inscripción exitosa para el Curso " + inscripcion.getCurso().getNombre() );
                message.setText("Estimado(a) " + newUser.getLogin() + ",<br/><br/>La inscripción para el Curso: <b>" + inscripcion.getCurso().getNombre() + "</b>" +
                    " ha sido exitosa." +
                    "<br/><br/>Puede ingresar en <a href=\"https://guaire.net/cursos/?inscripcion/user\">guaire.net</a><br/>" +
                    "<br/>Usuario: " + inscripcion.getAlumno().getEmail() +
                    "<br/>Clave: " + newPassword +
                    "<br/><br/>Atentamente,<br/>El equipo de Guaire Cursos", true);

                javaMailSender.send(mimeMessage);
            }
        }else{
            result = inscripcionRepository.save(inscripcion);

            for(Pago pago : inscripcion.getPagos()){
                if (pago.getMonto() != null && pago.getMonto() != 0) {
                    pago.setInscripcion(inscripcion);
                    pago.setFecha(Instant.now());
                    if(pago.getTipoPago().getTipoCuenta().getId() == 3751L){
                        pago.setEstatusPago(estatusPagoRepository.findById(3L).get());
                    }if(pago.getTipoPago().getTipoCuenta().getId() == 2601L){
                        pago.setEstatusPago(estatusPagoRepository.findById(1L).get());
                    }else if (pago.getTipoPago().getTipoCuenta().getId() == 2L){
                        pago.setEstatusPago(estatusPagoRepository.findById(1L).get());
                    } else if (pago.getTipoPago().getTipoCuenta().getId() == 1L){
                        pago.setEstatusPago(estatusPagoRepository.findById(2L).get());
                    }
                    pagoRepository.save(pago);
                }

            }

            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, CharEncoding.UTF_8);
            User user = userRepository.findOneByLogin(principal.getName()).get();
            message.setTo(user.getEmail());
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject("\"Inscripción exitosa para el Curso " + inscripcion.getCurso().getNombre() );
            message.setText("Estimado(a) " + user.getLogin() + ",<br/><br/>La inscripción para el Curso: <b>" + inscripcion.getCurso().getNombre() + "</b>" +
                " ha sido exitosa. Le enviaremos un correo de notificación el día anterior al Curso." +
                "<br/><br/>Puede ingresar en <a href=\"https://guaire.net/cursos/?inscripcion/user\">guaire.net</a>" +
                "<br/><br/>Atentamente,<br/>El equipo de Guaire Cursos", true);

            javaMailSender.send(mimeMessage);
        }

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, CharEncoding.UTF_8);
        User user = inscripcion.getCurso().getExpositor();
        message.setTo(user.getEmail());
        message.setFrom(jHipsterProperties.getMail().getFrom());
        message.setSubject("\"Inscripción exitosa para el Curso " + inscripcion.getCurso().getNombre() );
        message.setText("Estimado(a) " + user.getLogin() + ",<br/><br/>La inscripción para el Curso: <b>" + inscripcion.getCurso().getNombre() + "</b>" +
            " por parte del usuario " + inscripcion.getAlumno().getEmail() + "ha sido exitosa." +
            "<br/><br/>Atentamente,<br/>El equipo de Guaire Cursos", true);

        javaMailSender.send(mimeMessage);

        return ResponseEntity.created(new URI("/api/inscripcions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /inscripcions : Updates an existing inscripcion.
     *
     * @param inscripcion the inscripcion to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated inscripcion,
     * or with status 400 (Bad Request) if the inscripcion is not valid,
     * or with status 500 (Internal Server Error) if the inscripcion couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/inscripcions")
    public ResponseEntity<Inscripcion> updateInscripcion(@Valid @RequestBody Inscripcion inscripcion) throws URISyntaxException {
        log.debug("REST request to update Inscripcion : {}", inscripcion);
        if (inscripcion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Inscripcion result = inscripcionRepository.save(inscripcion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, inscripcion.getId().toString()))
            .body(result);
    }

    /**
     * GET  /inscripcions : get all the inscripcions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of inscripcions in body
     */
    @GetMapping("/inscripcions")
    public ResponseEntity<List<Inscripcion>> getAllInscripcions(Pageable pageable) {
        log.debug("REST request to get a page of Inscripcions");
        Page<Inscripcion> page = inscripcionRepository.findAll(pageable);

        addPaymentInformation(page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inscripcions");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /inscripcions : get the inscripcions from user.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of inscripcions in body
     */
    @GetMapping("/inscripcions/user")
    public ResponseEntity<List<Inscripcion>> getAllUserInscripcions(Pageable pageable) {
        log.debug("REST request to get a page of Inscripcions");
        Page<Inscripcion> page = inscripcionRepository.findByAlumnoIsCurrentUser(pageable);

        addPaymentInformation(page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inscripcions");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /inscripcions : get the inscripcions from course.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of inscripcions in body
     */
    @GetMapping("/inscripcions/curso/{id}")
    public ResponseEntity<List<Inscripcion>> getAllCourseInscripcions(@PathVariable Long id, Pageable pageable) {
        log.debug("REST request to get a page of Inscripcions");
        Page<Inscripcion> page = inscripcionRepository.findByCurso_Id(id, pageable);

        addPaymentInformation(page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/inscripcions");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /inscripcions/:id : get the "id" inscripcion.
     *
     * @param id the id of the inscripcion to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the inscripcion, or with status 404 (Not Found)
     */
    @GetMapping("/inscripcions/{id}")
    public ResponseEntity<Inscripcion> getInscripcion(@PathVariable Long id) {
        log.debug("REST request to get Inscripcion : {}", id);
        Optional<Inscripcion> inscripcion = inscripcionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(inscripcion);
    }

    /**
     * DELETE  /inscripcions/:id : delete the "id" inscripcion.
     *
     * @param id the id of the inscripcion to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/inscripcions/{id}")
    public ResponseEntity<Void> deleteInscripcion(@PathVariable Long id) {
        log.debug("REST request to delete Inscripcion : {}", id);
        inscripcionRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    public void addPaymentInformation(List<Inscripcion> inscripciones){
        for(Inscripcion inscripcion : inscripciones){
            inscripcion.setPagos(pagoRepository.findByInscripcion_Id(inscripcion.getId()));

            inscripcion.getCurso().setImagen(null);

            for(Pago pago : inscripcion.getPagos()){
                if(pago.getEstatusPago() == null){
                    if(pago.getTipoPago().getTipoCuenta().getId() == 3751L){
                        pago.setEstatusPago(estatusPagoRepository.findById(3L).get());
                    }if(pago.getTipoPago().getTipoCuenta().getId() == 2601L){
                        pago.setEstatusPago(estatusPagoRepository.findById(1L).get());
                    }else if (pago.getTipoPago().getTipoCuenta().getId() == 2L){
                        pago.setEstatusPago(estatusPagoRepository.findById(1L).get());
                    } else if (pago.getTipoPago().getTipoCuenta().getId() == 1L){
                        pago.setEstatusPago(estatusPagoRepository.findById(2L).get());
                    }
                    pagoRepository.save(pago);
                }
                pago.setInscripcion(null);
            }
        }
    }
}
