package com.guaire.guairecursos.web.rest;
import com.guaire.guairecursos.domain.TipoCuenta;
import com.guaire.guairecursos.repository.TipoCuentaRepository;
import com.guaire.guairecursos.web.rest.errors.BadRequestAlertException;
import com.guaire.guairecursos.web.rest.util.HeaderUtil;
import com.guaire.guairecursos.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TipoCuenta.
 */
@RestController
@RequestMapping("/api")
public class TipoCuentaResource {

    private final Logger log = LoggerFactory.getLogger(TipoCuentaResource.class);

    private static final String ENTITY_NAME = "tipoCuenta";

    private final TipoCuentaRepository tipoCuentaRepository;

    public TipoCuentaResource(TipoCuentaRepository tipoCuentaRepository) {
        this.tipoCuentaRepository = tipoCuentaRepository;
    }

    /**
     * POST  /tipo-cuentas : Create a new tipoCuenta.
     *
     * @param tipoCuenta the tipoCuenta to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoCuenta, or with status 400 (Bad Request) if the tipoCuenta has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-cuentas")
    public ResponseEntity<TipoCuenta> createTipoCuenta(@Valid @RequestBody TipoCuenta tipoCuenta) throws URISyntaxException {
        log.debug("REST request to save TipoCuenta : {}", tipoCuenta);
        if (tipoCuenta.getId() != null) {
            throw new BadRequestAlertException("A new tipoCuenta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoCuenta result = tipoCuentaRepository.save(tipoCuenta);
        return ResponseEntity.created(new URI("/api/tipo-cuentas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-cuentas : Updates an existing tipoCuenta.
     *
     * @param tipoCuenta the tipoCuenta to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoCuenta,
     * or with status 400 (Bad Request) if the tipoCuenta is not valid,
     * or with status 500 (Internal Server Error) if the tipoCuenta couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-cuentas")
    public ResponseEntity<TipoCuenta> updateTipoCuenta(@Valid @RequestBody TipoCuenta tipoCuenta) throws URISyntaxException {
        log.debug("REST request to update TipoCuenta : {}", tipoCuenta);
        if (tipoCuenta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TipoCuenta result = tipoCuentaRepository.save(tipoCuenta);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoCuenta.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipo-cuentas : get all the tipoCuentas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tipoCuentas in body
     */
    @GetMapping("/tipo-cuentas")
    public ResponseEntity<List<TipoCuenta>> getAllTipoCuentas(Pageable pageable) {
        log.debug("REST request to get a page of TipoCuentas");
        Page<TipoCuenta> page = tipoCuentaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tipo-cuentas");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /tipo-cuentas/:id : get the "id" tipoCuenta.
     *
     * @param id the id of the tipoCuenta to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipoCuenta, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-cuentas/{id}")
    public ResponseEntity<TipoCuenta> getTipoCuenta(@PathVariable Long id) {
        log.debug("REST request to get TipoCuenta : {}", id);
        Optional<TipoCuenta> tipoCuenta = tipoCuentaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(tipoCuenta);
    }

    /**
     * DELETE  /tipo-cuentas/:id : delete the "id" tipoCuenta.
     *
     * @param id the id of the tipoCuenta to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-cuentas/{id}")
    public ResponseEntity<Void> deleteTipoCuenta(@PathVariable Long id) {
        log.debug("REST request to delete TipoCuenta : {}", id);
        tipoCuentaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
