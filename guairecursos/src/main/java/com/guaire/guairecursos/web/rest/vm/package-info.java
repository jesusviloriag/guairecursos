/**
 * View Models used by Spring MVC REST controllers.
 */
package com.guaire.guairecursos.web.rest.vm;
