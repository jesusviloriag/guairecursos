package com.guaire.guairecursos.web.rest;
import com.guaire.guairecursos.domain.Moneda;
import com.guaire.guairecursos.repository.MonedaRepository;
import com.guaire.guairecursos.web.rest.errors.BadRequestAlertException;
import com.guaire.guairecursos.web.rest.util.HeaderUtil;
import com.guaire.guairecursos.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Moneda.
 */
@RestController
@RequestMapping("/api")
public class MonedaResource {

    private final Logger log = LoggerFactory.getLogger(MonedaResource.class);

    private static final String ENTITY_NAME = "moneda";

    private final MonedaRepository monedaRepository;

    public MonedaResource(MonedaRepository monedaRepository) {
        this.monedaRepository = monedaRepository;
    }

    /**
     * POST  /monedas : Create a new moneda.
     *
     * @param moneda the moneda to create
     * @return the ResponseEntity with status 201 (Created) and with body the new moneda, or with status 400 (Bad Request) if the moneda has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/monedas")
    public ResponseEntity<Moneda> createMoneda(@Valid @RequestBody Moneda moneda) throws URISyntaxException {
        log.debug("REST request to save Moneda : {}", moneda);
        if (moneda.getId() != null) {
            throw new BadRequestAlertException("A new moneda cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Moneda result = monedaRepository.save(moneda);
        return ResponseEntity.created(new URI("/api/monedas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /monedas : Updates an existing moneda.
     *
     * @param moneda the moneda to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated moneda,
     * or with status 400 (Bad Request) if the moneda is not valid,
     * or with status 500 (Internal Server Error) if the moneda couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/monedas")
    public ResponseEntity<Moneda> updateMoneda(@Valid @RequestBody Moneda moneda) throws URISyntaxException {
        log.debug("REST request to update Moneda : {}", moneda);
        if (moneda.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Moneda result = monedaRepository.save(moneda);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, moneda.getId().toString()))
            .body(result);
    }

    /**
     * GET  /monedas : get all the monedas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of monedas in body
     */
    @GetMapping("/monedas")
    public ResponseEntity<List<Moneda>> getAllMonedas(Pageable pageable) {
        log.debug("REST request to get a page of Monedas");
        Page<Moneda> page = monedaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/monedas");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /monedas/:id : get the "id" moneda.
     *
     * @param id the id of the moneda to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the moneda, or with status 404 (Not Found)
     */
    @GetMapping("/monedas/{id}")
    public ResponseEntity<Moneda> getMoneda(@PathVariable Long id) {
        log.debug("REST request to get Moneda : {}", id);
        Optional<Moneda> moneda = monedaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(moneda);
    }

    /**
     * DELETE  /monedas/:id : delete the "id" moneda.
     *
     * @param id the id of the moneda to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/monedas/{id}")
    public ResponseEntity<Void> deleteMoneda(@PathVariable Long id) {
        log.debug("REST request to delete Moneda : {}", id);
        monedaRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
