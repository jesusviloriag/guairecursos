package com.guaire.guairecursos.repository;

import com.guaire.guairecursos.domain.Pago;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;


/**
 * Spring Data  repository for the Pago entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PagoRepository extends JpaRepository<Pago, Long> {
    Set<Pago> findByInscripcion_Id(Long id);
}
