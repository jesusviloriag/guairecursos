package com.guaire.guairecursos.repository;

import com.guaire.guairecursos.domain.Inscripcion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Inscripcion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

    @Query("select inscripcion from Inscripcion inscripcion where inscripcion.alumno.login = ?#{principal.username}")
    List<Inscripcion> findByAlumnoIsCurrentUser();

    @Query("select inscripcion from Inscripcion inscripcion where inscripcion.alumno.login = ?#{principal.username}")
    Page<Inscripcion> findByAlumnoIsCurrentUser(Pageable pageable);

    Page<Inscripcion> findByCurso_Id(Long id, Pageable pageable);

}
