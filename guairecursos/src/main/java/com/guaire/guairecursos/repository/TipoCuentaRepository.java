package com.guaire.guairecursos.repository;

import com.guaire.guairecursos.domain.TipoCuenta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TipoCuenta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoCuentaRepository extends JpaRepository<TipoCuenta, Long> {

}
