package com.guaire.guairecursos.repository;

import com.guaire.guairecursos.domain.Curso;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Curso entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CursoRepository extends JpaRepository<Curso, Long> {

    @Query("select curso from Curso curso where curso.expositor.login = ?#{principal.username}")
    List<Curso> findByExpositorIsCurrentUser();

    @Query(value = "select distinct curso from Curso curso left join fetch curso.tipoPagos",
        countQuery = "select count(distinct curso) from Curso curso")
    Page<Curso> findAllWithEagerRelationships(Pageable pageable);

    @Query(value = "select distinct curso from Curso curso left join fetch curso.tipoPagos")
    List<Curso> findAllWithEagerRelationships();

    @Query("select curso from Curso curso left join fetch curso.tipoPagos where curso.id =:id")
    Optional<Curso> findOneWithEagerRelationships(@Param("id") Long id);

}
