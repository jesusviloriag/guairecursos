package com.guaire.guairecursos.repository;

import com.guaire.guairecursos.domain.TipoPago;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the TipoPago entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoPagoRepository extends JpaRepository<TipoPago, Long> {

}
