package com.guaire.guairecursos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Pago.
 */
@Entity
@Table(name = "pago")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pago implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private Instant fecha;

    @NotNull
    @Column(name = "monto", nullable = false)
    private Float monto;

    @Lob
    @Column(name = "foto_comprobante")
    private byte[] fotoComprobante;

    @Column(name = "foto_comprobante_content_type")
    private String fotoComprobanteContentType;

    @Column(name = "fecha_status")
    private Instant fechaStatus;

    @Column(name = "numero_transaccion")
    private String numeroTransaccion;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("pagos")
    private TipoPago tipoPago;

    @ManyToOne
    @JsonIgnoreProperties("pagos")
    private EstatusPago estatusPago;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("pagos")
    private Moneda moneda;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("pagos")
    private Inscripcion inscripcion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFecha() {
        return fecha;
    }

    public Pago fecha(Instant fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }

    public Float getMonto() {
        return monto;
    }

    public Pago monto(Float monto) {
        this.monto = monto;
        return this;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

    public byte[] getFotoComprobante() {
        return fotoComprobante;
    }

    public Pago fotoComprobante(byte[] fotoComprobante) {
        this.fotoComprobante = fotoComprobante;
        return this;
    }

    public void setFotoComprobante(byte[] fotoComprobante) {
        this.fotoComprobante = fotoComprobante;
    }

    public String getFotoComprobanteContentType() {
        return fotoComprobanteContentType;
    }

    public Pago fotoComprobanteContentType(String fotoComprobanteContentType) {
        this.fotoComprobanteContentType = fotoComprobanteContentType;
        return this;
    }

    public void setFotoComprobanteContentType(String fotoComprobanteContentType) {
        this.fotoComprobanteContentType = fotoComprobanteContentType;
    }

    public Instant getFechaStatus() {
        return fechaStatus;
    }

    public Pago fechaStatus(Instant fechaStatus) {
        this.fechaStatus = fechaStatus;
        return this;
    }

    public void setFechaStatus(Instant fechaStatus) {
        this.fechaStatus = fechaStatus;
    }

    public String getNumeroTransaccion() {
        return numeroTransaccion;
    }

    public Pago numeroTransaccion(String numeroTransaccion) {
        this.numeroTransaccion = numeroTransaccion;
        return this;
    }

    public void setNumeroTransaccion(String numeroTransaccion) {
        this.numeroTransaccion = numeroTransaccion;
    }

    public TipoPago getTipoPago() {
        return tipoPago;
    }

    public Pago tipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
        return this;
    }

    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }

    public EstatusPago getEstatusPago() {
        return estatusPago;
    }

    public Pago estatusPago(EstatusPago estatusPago) {
        this.estatusPago = estatusPago;
        return this;
    }

    public void setEstatusPago(EstatusPago estatusPago) {
        this.estatusPago = estatusPago;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public Pago moneda(Moneda moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Inscripcion getInscripcion() {
        return inscripcion;
    }

    public Pago inscripcion(Inscripcion inscripcion) {
        this.inscripcion = inscripcion;
        return this;
    }

    public void setInscripcion(Inscripcion inscripcion) {
        this.inscripcion = inscripcion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Pago pago = (Pago) o;
        if (pago.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pago.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Pago{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", monto=" + getMonto() +
            ", fotoComprobante='" + getFotoComprobante() + "'" +
            ", fotoComprobanteContentType='" + getFotoComprobanteContentType() + "'" +
            ", fechaStatus='" + getFechaStatus() + "'" +
            ", numeroTransaccion='" + getNumeroTransaccion() + "'" +
            "}";
    }
}
