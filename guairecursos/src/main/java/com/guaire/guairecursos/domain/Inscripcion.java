package com.guaire.guairecursos.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Inscripcion.
 */
@Entity
@Table(name = "inscripcion")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Inscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private Instant fecha;

    @Column(name = "comentarios")
    private String comentarios;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("inscripcions")
    private Curso curso;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("inscripcions")
    private User alumno;

    @OneToMany(mappedBy = "inscripcion")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Pago> pagos = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFecha() {
        return fecha;
    }

    public Inscripcion fecha(Instant fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }

    public String getComentarios() {
        return comentarios;
    }

    public Inscripcion comentarios(String comentarios) {
        this.comentarios = comentarios;
        return this;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Curso getCurso() {
        return curso;
    }

    public Inscripcion curso(Curso curso) {
        this.curso = curso;
        return this;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public User getAlumno() {
        return alumno;
    }

    public Inscripcion alumno(User user) {
        this.alumno = user;
        return this;
    }

    public void setAlumno(User user) {
        this.alumno = user;
    }

    public Set<Pago> getPagos() {
        return pagos;
    }

    public Inscripcion pagos(Set<Pago> pagos) {
        this.pagos = pagos;
        return this;
    }

    public Inscripcion addPagos(Pago pago) {
        this.pagos.add(pago);
        pago.setInscripcion(this);
        return this;
    }

    public Inscripcion removePagos(Pago pago) {
        this.pagos.remove(pago);
        pago.setInscripcion(null);
        return this;
    }

    public void setPagos(Set<Pago> pagos) {
        this.pagos = pagos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Inscripcion inscripcion = (Inscripcion) o;
        if (inscripcion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), inscripcion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Inscripcion{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", comentarios='" + getComentarios() + "'" +
            "}";
    }
}
