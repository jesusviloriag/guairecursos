package com.guaire.guairecursos.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Moneda.
 */
@Entity
@Table(name = "moneda")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Moneda implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @NotNull
    @Column(name = "simbolo", nullable = false)
    private String simbolo;

    @NotNull
    @Column(name = "cambio", nullable = false)
    private Float cambio;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Moneda nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public Moneda simbolo(String simbolo) {
        this.simbolo = simbolo;
        return this;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public Float getCambio() {
        return cambio;
    }

    public Moneda cambio(Float cambio) {
        this.cambio = cambio;
        return this;
    }

    public void setCambio(Float cambio) {
        this.cambio = cambio;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Moneda moneda = (Moneda) o;
        if (moneda.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), moneda.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Moneda{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", simbolo='" + getSimbolo() + "'" +
            ", cambio=" + getCambio() +
            "}";
    }
}
