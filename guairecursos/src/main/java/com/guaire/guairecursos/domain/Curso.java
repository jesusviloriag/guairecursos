package com.guaire.guairecursos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Curso.
 */
@Entity
@Table(name = "curso")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Curso implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @NotNull
    @Column(name = "lugar", nullable = false)
    private String lugar;

    @Lob
    @Column(name = "imagen")
    private byte[] imagen;

    @Column(name = "imagen_content_type")
    private String imagenContentType;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private ZonedDateTime fecha;

    @NotNull
    @Column(name = "hora_inicio", nullable = false)
    private Integer horaInicio;

    @Column(name = "minuto_inicio")
    private Integer minutoInicio;

    @NotNull
    @Column(name = "hora_fin", nullable = false)
    private Integer horaFin;

    @Column(name = "minuto_fin")
    private Integer minutoFin;

    @Column(name = "cupos")
    private Integer cupos;

    @Column(name = "link_informativo")
    private String linkInformativo;

    @NotNull
    @Column(name = "monto", nullable = false)
    private Float monto;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("cursos")
    private User expositor;

    @ManyToOne
    @JsonIgnoreProperties("cursos")
    private Moneda moneda;

    @ManyToMany(fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @NotNull
    @JoinTable(name = "curso_tipo_pago",
               joinColumns = @JoinColumn(name = "curso_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "tipo_pago_id", referencedColumnName = "id"))
    private Set<TipoPago> tipoPagos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Curso nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Curso descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLugar() {
        return lugar;
    }

    public Curso lugar(String lugar) {
        this.lugar = lugar;
        return this;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public Curso imagen(byte[] imagen) {
        this.imagen = imagen;
        return this;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getImagenContentType() {
        return imagenContentType;
    }

    public Curso imagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
        return this;
    }

    public void setImagenContentType(String imagenContentType) {
        this.imagenContentType = imagenContentType;
    }

    public ZonedDateTime getFecha() {
        return fecha;
    }

    public Curso fecha(ZonedDateTime fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(ZonedDateTime fecha) {
        this.fecha = fecha;
    }

    public Integer getHoraInicio() {
        return horaInicio;
    }

    public Curso horaInicio(Integer horaInicio) {
        this.horaInicio = horaInicio;
        return this;
    }

    public void setHoraInicio(Integer horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Integer getMinutoInicio() {
        return minutoInicio;
    }

    public Curso minutoInicio(Integer minutoInicio) {
        this.minutoInicio = minutoInicio;
        return this;
    }

    public void setMinutoInicio(Integer minutoInicio) {
        this.minutoInicio = minutoInicio;
    }

    public Integer getHoraFin() {
        return horaFin;
    }

    public Curso horaFin(Integer horaFin) {
        this.horaFin = horaFin;
        return this;
    }

    public void setHoraFin(Integer horaFin) {
        this.horaFin = horaFin;
    }

    public Integer getMinutoFin() {
        return minutoFin;
    }

    public Curso minutoFin(Integer minutoFin) {
        this.minutoFin = minutoFin;
        return this;
    }

    public void setMinutoFin(Integer minutoFin) {
        this.minutoFin = minutoFin;
    }

    public String getLinkInformativo() {
        return linkInformativo;
    }

    public Curso linkInformativo(String linkInformativo) {
        this.linkInformativo = linkInformativo;
        return this;
    }

    public void setLinkInformativo(String linkInformativo) {
        this.linkInformativo = linkInformativo;
    }

    public Float getMonto() {
        return monto;
    }

    public Curso monto(Float monto) {
        this.monto = monto;
        return this;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

    public User getExpositor() {
        return expositor;
    }

    public Curso expositor(User user) {
        this.expositor = user;
        return this;
    }

    public void setExpositor(User user) {
        this.expositor = user;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public Curso moneda(Moneda moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public Set<TipoPago> getTipoPagos() {
        return tipoPagos;
    }

    public Curso tipoPagos(Set<TipoPago> tipoPagos) {
        this.tipoPagos = tipoPagos;
        return this;
    }

    public Curso addTipoPago(TipoPago tipoPago) {
        this.tipoPagos.add(tipoPago);
        return this;
    }

    public Curso removeTipoPago(TipoPago tipoPago) {
        this.tipoPagos.remove(tipoPago);
        return this;
    }

    public void setTipoPagos(Set<TipoPago> tipoPagos) {
        this.tipoPagos = tipoPagos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Curso curso = (Curso) o;
        if (curso.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), curso.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Curso{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", lugar='" + getLugar() + "'" +
            ", imagen='" + getImagen() + "'" +
            ", imagenContentType='" + getImagenContentType() + "'" +
            ", fecha='" + getFecha() + "'" +
            ", horaInicio=" + getHoraInicio() +
            ", minutoInicio=" + getMinutoInicio() +
            ", horaFin=" + getHoraFin() +
            ", minutoFin=" + getMinutoFin() +
            ", linkInformativo='" + getLinkInformativo() + "'" +
            ", monto=" + getMonto() +
            "}";
    }

    public Integer getCupos() {
        return cupos;
    }

    public void setCupos(Integer cupos) {
        this.cupos = cupos;
    }
}
