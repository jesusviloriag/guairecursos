package com.guaire.guairecursos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TipoPago.
 */
@Entity
@Table(name = "tipo_pago")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TipoPago implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @NotNull
    @Column(name = "instantaneo", nullable = false)
    private Boolean instantaneo;

    @NotNull
    @Column(name = "cuenta", nullable = false)
    private String cuenta;

    @Column(name = "email")
    private String email;

    @Column(name = "cedula")
    private String cedula;

    @Column(name = "titular")
    private String titular;

    @Column(name = "script_boton")
    private String scriptBoton;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tipoPagos")
    private Moneda moneda;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("tipoPagos")
    private TipoCuenta tipoCuenta;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public TipoPago nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean isInstantaneo() {
        return instantaneo;
    }

    public TipoPago instantaneo(Boolean instantaneo) {
        this.instantaneo = instantaneo;
        return this;
    }

    public void setInstantaneo(Boolean instantaneo) {
        this.instantaneo = instantaneo;
    }

    public String getCuenta() {
        return cuenta;
    }

    public TipoPago cuenta(String cuenta) {
        this.cuenta = cuenta;
        return this;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getEmail() {
        return email;
    }

    public TipoPago email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCedula() {
        return cedula;
    }

    public TipoPago cedula(String cedula) {
        this.cedula = cedula;
        return this;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTitular() {
        return titular;
    }

    public TipoPago titular(String titular) {
        this.titular = titular;
        return this;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getScriptBoton() {
        return scriptBoton;
    }

    public TipoPago scriptBoton(String scriptBoton) {
        this.scriptBoton = scriptBoton;
        return this;
    }

    public void setScriptBoton(String scriptBoton) {
        this.scriptBoton = scriptBoton;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public TipoPago moneda(Moneda moneda) {
        this.moneda = moneda;
        return this;
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public TipoCuenta getTipoCuenta() {
        return tipoCuenta;
    }

    public TipoPago tipoCuenta(TipoCuenta tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
        return this;
    }

    public void setTipoCuenta(TipoCuenta tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoPago tipoPago = (TipoPago) o;
        if (tipoPago.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoPago.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoPago{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", instantaneo='" + isInstantaneo() + "'" +
            ", cuenta='" + getCuenta() + "'" +
            ", email='" + getEmail() + "'" +
            ", cedula='" + getCedula() + "'" +
            ", titular='" + getTitular() + "'" +
            ", scriptBoton='" + getScriptBoton() + "'" +
            "}";
    }
}
